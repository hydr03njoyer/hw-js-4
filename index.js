function mathOperation (num1 , num2 , operation) {
    let result;
    if (operation === "+") {
        result = num1 + num2 ;
    } else if (operation === "-") {
        result = num1 - num2 ;
    } else if (operation === "*") {
        result = num1 * num2;
    } else if (operation === "/") {
        result = num1 / num2;
    }
    return result ;
}

let num1,
    num2,
    operation;
while (true) {
    num1 = Number(prompt("Write first number:"))
    num2 = Number(prompt("Write second number:"))
    operation = prompt("Write operation (+, -, *, /) :")
    if (isNaN(num1) || isNaN(num2)) {
        alert("Write correct numbers!");
    } else if (!["+", "-", "*", "/"].includes(operation)){
        alert("Write correct operation!");
    }
    else {
    break;
    }
}

const result = mathOperation(num1, num2, operation) ;
console.log(`${result}`) ;
let userResult = alert(`${result}`) ;

